FROM php:8.2-fpm-bookworm

LABEL Maintainer="Komunitas WordPress Indonesia" \
	Description="WordPress on PHP-FPM 8.2 / Debian Linux."

# Configure WP-CLI
ENV WP_CLI_CONFIG_PATH=/var/www/wp-cli.yml

# Install packages
RUN export DEBIAN_FRONTEND=noninteractive && \
	apt-get update && \
	apt-get upgrade -yqq && \
	apt-get install -yqq --no-install-recommends \
	ghostscript \
	libltdl7 \
	libmagickcore-6.q16-6 \
	libmagickwand-6.q16-6 \
	libmcrypt4 \
	libzip4 \
	zlib1g \
	# Required by WP-CLI
	less \
	mariadb-client \
	# Mail
	msmtp \
	&& \
	BUILD_DEPS=" \
	libmagickcore-dev \
	libmagickwand-dev \
	libmcrypt-dev \
	libtool \
	libwebp-dev \
	libzip-dev \
	libpcre2-dev \
	zlib1g-dev \
	" && \
	# Install build dependencies.
	apt-get install -yqq --no-install-recommends $BUILD_DEPS && \
	NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
	INC_DIR=/usr/include && \
	docker-php-source extract && \
	pear channel-update pear.php.net && \
	pecl channel-update pecl.php.net && \
	PECL_PACKAGES="\
	imagick \
	redis \
	" && \
	# Install PHP modules.
	pecl install $PECL_PACKAGES && \
	docker-php-ext-enable $PECL_PACKAGES && \
	docker-php-ext-configure gd \
	--enable-gd \
	--with-freetype=${INC_DIR} \
	--with-jpeg=${INC_DIR} \
	--with-webp=${INC_DIR} && \
	docker-php-ext-configure pdo_mysql --with-pdo-mysql && \
	docker-php-ext-configure zip --with-zip && \
	docker-php-ext-install -j${NPROC} \
	bcmath \
	exif \
	gd \
	intl \
	mysqli \
	opcache \
	pcntl \
	pdo_mysql \
	zip \
	&& \
	# Clean-ups.
	apt-get remove -yqq $BUILD_DEPS && \
	apt-get -yqq autoremove && \
	apt-get clean && \
	docker-php-source delete && \
	rm -rf \
	/tmp/* \
	~/.pearrc

# Install WP-CLI & WordPress.
ARG WP_CLI_VERSION=2.10.0
RUN curl -o /usr/local/bin/wp -SL "https://github.com/wp-cli/wp-cli/releases/download/v${WP_CLI_VERSION}/wp-cli-${WP_CLI_VERSION}.phar" && \
	chmod +x /usr/local/bin/wp

# Setup WordPress.
ARG WP_VERSION=6.5.3
RUN php -d memory_limit=512M /usr/local/bin/wp core download --allow-root --force --path=/var/www/wordpress --version="${WP_VERSION}" && \
	mv /var/www/wordpress/wp-content/themes /var/www/wordpress/default-themes && \
	# Deprecated.
	mkdir -p /var/www/wp-configs && \
	# Remove WP source file.
	rm -rf /root/.wp-cli

# WP & WP-CLI configs
COPY config/wordpress/* /var/www/

# PHP configs
COPY config/php/* $PHP_INI_DIR/conf.d/
COPY config/php-fpm.d/* /usr/local/etc/php-fpm.d/

# Misc. configs
COPY config/etc/* /etc/

VOLUME /var/www/wp-content /var/www/wordpress

WORKDIR /var/www/wordpress

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]

CMD [ "php-fpm" ]
