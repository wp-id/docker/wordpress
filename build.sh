#!/bin/sh

set -e

REGISTRY_PASS="${DOCKER_PASSWORD}"
REGISTRY_USER="${DOCKER_USER}"
TAG=latest

if [ -z ${1+x} ]; then
    # Main image.
    DOCKERFILE=Dockerfile
    PREV_IMAGE_NAME="${IMAGE_NAME}:latest"
    BUILD_IMAGE_NAME="${IMAGE_NAME}:${TAG}"
else
    # Variants.
    DOCKERFILE="variants/${1}/Dockerfile"
    PREV_IMAGE_NAME="${IMAGE_NAME}:${1}"

    if [ $TAG = "latest" ]; then
        BUILD_IMAGE_NAME="${PREV_IMAGE_NAME}"
    else
        BUILD_IMAGE_NAME="${IMAGE_NAME}:${TAG}-${1}"
    fi
fi

echo $DOCKER_PASSWORD | docker login -u $DOCKER_USER --password-stdin

docker pull $PREV_IMAGE_NAME || true
eval "docker build --cache-from ${PREV_IMAGE_NAME} -f ${DOCKERFILE} -t ${BUILD_IMAGE_NAME} ."

if [ $CI_COMMIT_REF_NAME = "main" ]; then
	docker push $BUILD_IMAGE_NAME
fi
