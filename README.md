# WordPress Docker Container

WordPress on PHP-FPM 8.2

_WordPress version currently installed:_ **6.5.3**

## Usage

```sh
wget https://gitlab.com/wp-id/docker/wpc/raw/master/config/nginx.conf
mkdir -p data/db && \
mkdir -p data/wp-content/uploads && \
docker run -d \
    --name=wordpress_db \
    -e MYSQL_ROOT_PASSWORD=password \
    -e MYSQL_DATABASE=wordpress \
    -e MYSQL_USER=wordpress \
    -e MYSQL_PASSWORD=wordpress \
    -v `pwd`/data/db:/var/lib/mysql \
    mariadb && \
docker run -d \
    --name=wordpress \
    --link wordpress_db \
    -e DB_HOST=wordpress_db \
    -e DB_NAME=wordpress \
    -e DB_USER=wordpress \
    -e DB_PASSWORD=wordpress \
    -e SMTP_HOST=smtp.example.com \
    -e SMTP_PORT=587 \
    -e SMTP_PASS=yourapplicationpassword \
    -e SMTP_USER=yourusername@example.com \
    -e SMTP_PASS=yourapplicationpassword \
    -v `pwd`/data/wp-content:/var/www/wp-content \
    wpid/wordpress && \
docker run -d \
    --name=wordpress_web \
    --link wordpress \
    -p "80:80" \
    -v `pwd`/config/nginx.conf:/etc/nginx/nginx.conf:ro \
    --volumes-from wordpress \
    nginx:alpine
```

...or take a look at [WordPress Compose](https://gitlab.com/wp-id/docker/wpc).

### NOTES

-   If provided, `SMTP_HOST`, `SMTP_PORT`, `SMTP_USER` & `SMTP_PASS` will be used to create msmtp config for sending out emails (eg. for password reset, comment notification, etc).
-   If you need [V8JS](https://github.com/phpv8/v8js) extension, use `wpid/wordpress:v8js` image instead.
