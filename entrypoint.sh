#!/bin/bash

# terminate on errors
set -e

cat << "EOF"

Brought to you by...

_ _ ____   ____  ____________
| | ||__]__||  \ |  ||__/| __
|_|_||     ||__/.|__||  \|__]

https://wp-id.org

EOF

mysql=( mysql -h ${DB_HOST} -u${DB_USER} -p${DB_PASSWORD} --skip-column-names )

# Stolen from XWP's wp-docker.
function is_db_up() {
  result=$( echo "SHOW DATABASES LIKE '${DB_NAME}'" | "${mysql[@]}" 2>/dev/null )

  if [ "$result" == "${DB_NAME}" ]; then
    return 0
  else
    return 1
  fi
}

# Stolen from https://hub.docker.com/u/binhex/
echo "[info] System information $(uname -a)"

mkdir -p /var/www/wp-content/{uploads,upgrade,languages}

# Set www-data's user ID.
if [ ! -z "${PUID}" ]; then
    echo "[info] PUID defined as '${PUID}'"
    # set user www-data to specified user id (non unique)
    usermod -o -u "${PUID}" www-data &>/dev/null
fi

# Set www-data's group ID.
if [ ! -z "${PGID}" ]; then
    echo "[info] PGID defined as '${PGID}'"
    # set group www-data to specified group id (non unique)
    groupmod -o -g "${PGID}" www-data &>/dev/null
fi

if [ ! -f "/var/www/wp-content/perms.txt" ]; then
    chown -R www-data:www-data /var/www/wp-content/{uploads,upgrade,languages}
    echo "Permission set." > /var/www/wp-content/perms.txt
fi

# Configure SMTP
if [ ! -z "${SMTP_USER}" ] && [ ! -z "${SMTP_PASS}" ] && [ ! -z "${SMTP_HOST}" ] && [ ! -z "${SMTP_PORT}" ]; then
    sed -e "s/SMTP_USER/${SMTP_USER}/g;s/SMTP_PASS/${SMTP_PASS}/g;s/SMTP_HOST/${SMTP_HOST}/g;s/SMTP_PORT/${SMTP_PORT}/g" \
        /etc/msmtprc.sample > /etc/msmtprc
    echo "MSMTP config created."
fi

until is_db_up; do
   echo "Waiting for database to become available..."
   sleep 3
done

echo "Database is available. Starting services..."

exec "$@"
