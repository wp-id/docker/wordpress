#!/bin/sh

# Variants.
if [ -z "${1+x}" ]; then
    LATEST_IMAGE_NAME="${IMAGE_NAME}:latest"
    NEW_IMAGE_TAG="${IMAGE_NAME}:${CI_COMMIT_TAG}"
else
    LATEST_IMAGE_NAME="${IMAGE_NAME}:${1}"
    NEW_IMAGE_TAG="${IMAGE_NAME}:${CI_COMMIT_TAG}-${1}"
fi

echo "$DOCKER_PASSWORD" | docker login $DOCKER_REGISTRY --username $DOCKER_USER --password-stdin

docker pull $LATEST_IMAGE_NAME
docker tag $LATEST_IMAGE_NAME $NEW_IMAGE_TAG
docker push $NEW_IMAGE_TAG
